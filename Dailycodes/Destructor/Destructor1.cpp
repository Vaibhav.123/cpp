
// eka constructor chya aatun dusrya constrcutor la call kela tr 2 objec bantat 
// tyamule ekane change kela tr dusryala nahi dist 
// fresh copy dili jaate


#include <iostream>

class Demo {

	public :
		int x = 10;
		Demo(){
		
			std::cout << "In Constructor" << std::endl;
			std::cout << x << std::endl;
		}

		Demo(int x) {
			
			this->x = x;

			std::cout << "In Para" << std::endl;
			std::cout << x << std::endl;

			Demo();
		}

		~ Demo(){
			
			std::cout << "In Destructor" << std::endl;
		}
};

int main(){

	Demo obj(50);
	std::cout << "End Main" << std::endl;
	return 0;
}
// OP :
// In Para
// 50
// In Constructor
// 10
// Destructor
// End Main
// Destructor
//
