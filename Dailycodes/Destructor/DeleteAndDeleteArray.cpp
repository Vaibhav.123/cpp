//  

#include <iostream>

class Demo {

	int *ptrArr = NULL;
	int *ptr = NULL;

	public :
		Demo(){
		
			ptrArr = new int[50];
			ptr = new int;
			*ptr = 100;
			std::cout << "In Constructor" << std::endl;
		}
		~Demo(){
		
			delete ptr;  // jrr fkt ek variable delete krycha asel tr
			
			delete[] ptrArr;  // array madhil one by one data delete krycha asel tr

			std::cout << "In Destructor" << std::endl;
		}
};

int main(){

	Demo obj;
	return 0;
}
