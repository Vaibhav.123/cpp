

#include<iostream>

int main(){

	int x = 10;
	
	int &y = x;

	int &z;     //  error: y declared as referenced but not initialized

	std::cout<< &x <<std::endl;   // 0x123456 
	std::cout<< &y <<std::endl;   // 0x123456
}
