

#include<iostream>

int main(){

	int x = 10;
	std::cout<< x <<std::endl;

	{
		int x = 20;
		std::cout<< x <<std::endl;
		std::cout<< ::x <<std::endl;  //error: x has not been declared [ local variable shadowing ] here x is not declared as globaly
		
		x = 30;
		
		std::cout<< x <<std::endl;
	}
	std::cout<< x <<std::endl;
}
