
//   this concept of name mangling
//  function ch name vegl jaat internally tyamul ambiguity nahi yet
//  pn jrr parameters equal astil tr abiguity yete 

#include <iostream>

int add(int x, int y){

	return x+y;
}

int add(int x, int y, int z){

	return x+y+z;
}

int main(){

	std::cout<< add(10,20) <<std::endl;  // internally this function call in diff [ _adii] asa jato
	
	std::cout<< add(10,20,30) <<std::endl;  // ani ha internally call vegla jato [ _adiii]  tyamul ithe ambiguity yet mahi 
}
