

#include<iostream>

int main(){

	int x = 10;
	int y = 20;

	std::cout<< x <<std::endl;  // 10
	std::cout<< y <<std::endl;  // 20

/*	const int *ptr = &x;

	std::cout<< *ptr <<std::endl;  // 10
 
	ptr = &y;
	std::cout<< *ptr <<std::endl;  // 20
	*/
	
/*	int const *ptr = &x;
	
	std::cout<< *ptr <<std::endl;  // 10
	
	ptr = &y;
	
	std::cout<< *ptr <<std::endl;  // 20

	*/

	//   above there is data constant 

	int const *const ptr = &x;
	
	std::cout<< x <<std::endl;
	
	ptr = &y;        // assignment of read only variable ptr 

	std::cout<< x <<std::endl;


}
