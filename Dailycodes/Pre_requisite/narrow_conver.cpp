

#include<iostream>

int main(){

	int x = 10;
	int y = 20.5f;

	std::cout<< x <<std::endl;
	std::cout<< y <<std::endl;

	int z{20.5f};  // error : narrowing conversion from float to int

	std::cout<< z <<std::endl;

}
