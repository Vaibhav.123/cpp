// MALLOC 

#include<stdio.h>
#include<stdlib.h>
void fun(){

	int *ptr = (int *)malloc(sizeof(int));

	*ptr = 20;

	printf("%d\n",*ptr);

	free(ptr);      //  free must required 

	printf("%d\n",*ptr);     //  garbage value 

}

void main(){
	
	int *ptr = (int *)malloc(sizeof(int));

	*ptr = 50;

	printf("%d\n",*ptr);

	free(ptr);     // free must required 
	
	printf("%d\n",*ptr);   // garbage value 

	fun();

}
