

// 3D array 


#include <stdio.h>

void fun(int (*ptr)[3][3],int size,int size1, int size2){

	for(int i=0; i<size; i++){
	
		for(int j=0; j<size1; j++){
		
			for(int k=0; k<size2; k++){
			
				printf("%d  ",*(*(*(ptr+i)+j)+k));
			}
			printf("\n");

		}
		printf("\n\n");
	}
}
void main(){

	int arr[][3][3] = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18};

	int size = sizeof(arr)/sizeof(arr[0]);
	int size1 = sizeof(arr[0])/sizeof(arr[0][0]);
	int size2 = sizeof(arr[0][0])/sizeof(arr[0][0][0]);
	
	fun(arr,size,size1,size2);
}
