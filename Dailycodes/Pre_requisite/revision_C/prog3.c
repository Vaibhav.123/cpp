
//  2D array
//
#include <stdio.h>


void fun(int (*ptr)[3],int size){
	
	for(int i=0; i<3; i++){
		for(int j=0; j<3; j++){
			printf("%d  ", *(*(ptr + i)+j));
		}
		printf("\n");
	}
}

void gun (int arr[][3],int size){

	//int size = sizeof(arr)/sizeof(arr[0]);

	for(int i=0; i<size; i++){
	
		for(int j=0; j<size; j++){
		
			printf("%d  ",arr[i][j]);
		}
		printf("\n");
	}
}

void main(){

	int arr[][3] = {1,2,3,4,5,6,7,8,9};
	
	int size = sizeof(arr)/sizeof(arr[0]);
	
	fun(arr,size);

	gun(arr,size);
}
