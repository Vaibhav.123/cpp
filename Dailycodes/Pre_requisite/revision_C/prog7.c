//   structure using object declaration


#include<stdio.h>
#include<stdlib.h>

struct Batsman  {

	char name[20];
	int jerNo;
	double avg;

}vk = {"Virat",18,48.60};

void main(){
	
	struct Batsman obj = {"Mahi",7,60.00};

	printf("%ld\n\n",sizeof(obj));
	
	//  simple way top access 
	
	printf("%s\n",vk.name);
	printf("%d\n",vk.jerNo);
	printf("%lf\n\n",vk.avg);

	printf("%s\n",obj.name);
	printf("%d\n",obj.jerNo);
	printf("%lf\n\n",obj.avg);
	
	//  using pointer access
	//
	
	struct Batsman *vptr = &vk;
	struct Batsman *mptr = &obj;

	// 1st way 

	printf("%s\n",vptr->name);
	printf("%d\n",vptr->jerNo);
	printf("%lf\n\n",vptr->avg);

	// 2nd way

	printf("%s\n",(*mptr).name);
	printf("%d\n",(*mptr).jerNo);
	printf("%lf\n",(*mptr).avg);
}
