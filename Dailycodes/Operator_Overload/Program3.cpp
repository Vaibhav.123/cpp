

#include <iostream>

class Demo {

	 int x = 10;

	public:
	 	Demo(){
		
		}
};

int main(){

	int a = 10;
	
	std::cout << x << std::endl;

	// operator << (cout, x)      => function call
	// prototype:
	// ostream& operator << (ostream& , int)   =>  predefined function
	

	Demo obj();

	std::cout << obj << std::endl;   // error : mo match found

	// operator << (cout, obj)      => function call
	// prototype:
	// ostream& operator << (ostream& , Demo&)   => not predefined function

}
