// in the below code 
// operator + is overloaded
// if we add two numbers then 
// internally it will called operator +() function 
// and then it return result
// here opearator overloading take part 
#include <iostream>

int main(){
	
	int x = 10;
	int y = 20;
	
	std::cout << x+y << std::endl;    // internally:  int operator + (int x , int y);

	return 0;
}
