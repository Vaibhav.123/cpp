// type no 2:
// using normal function 
// we can overload the operator

#include <iostream> 

class Demo {

	int x = 10;

	public :	
		Demo(int x) {
		
			this->x = x;
		}

		int getData() const{
		
			return x;
		}
};

int operator+ (const Demo& obj1, const Demo& obj2) {

	// return obj1.x + obj2.x;  // x is private
	return obj1.getData() + obj2.getData();
}
int main(){

	Demo obj1(20);
	Demo obj2(30);

	std::cout << obj1 + obj2 << std::endl;   // 50
	return 0;
}
