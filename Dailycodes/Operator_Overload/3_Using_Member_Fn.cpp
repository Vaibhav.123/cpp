// type 3:
// Using memeber function we overload the operator

#include <iostream> 

class Demo {

	int x = 10;
	int y = 20;
	public: 
		Demo(int x, int y){
		
			this->x = x;
			this->y = y;
		}

		int operator+ (const Demo& obj2){
		
			// return obj1.x + obj2.x;  error :no match found
			return obj2.y + this->x;
		}
		
};

int main(){

	Demo obj1(20,30);
	Demo obj2(40,50);
	
	std::cout << obj1+obj2 << std::endl;    // 70

	return 0;
}
