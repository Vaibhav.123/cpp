// here we can create Demo class object we want to print it then cannot print that
// because there is no predefined function like that 
// so error : no match for 'operator <<' (operand types are 'std::ostream' abd 'Demo')
//

#include <iostream>  

class Demo {

};

int main(){

	Demo obj;

	std::cout << obj << std::endl;   // internally:  operator << (cout,obj)
					 //
}
