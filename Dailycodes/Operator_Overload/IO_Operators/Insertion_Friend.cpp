// using friend function
//
#include <iostream>

class Demo {

	int x = 10;
	int y = 20;

	friend std::ostream& operator<<(std::ostream& cout, const Demo& obj);
	/*
		cout << obj.x;
		return cout;
	}
*/
};

std::ostream& operator<<(std::ostream& out, const Demo& obj){
	
	out<< obj.x;
	return out;
}

int main(){

	Demo obj;

	std::cout << obj << std::endl;

	return 0;
}
