// Extraction using Friend function

#include <iostream> 

class Demo {
	
	int x;
	int y;

	public:
		friend std::istream& operator>>(std::istream& cin, Demo& obj);

		void printData(){
		
			std::cout << x << "   " << y << std::endl;
		}
};

std::istream& operator>>(std::istream& cin, Demo& obj){

	cin>> obj.x;
	cin>> obj.y;
	return cin;
}

int main(){

	Demo obj;
	std::cout << "Enter value" << std::endl;

	std::cin>>obj;
	obj.printData();
	return 0;

}
