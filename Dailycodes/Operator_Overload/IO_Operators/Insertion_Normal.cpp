// using normal function 
// IO operator cannot overload using member function 
// beacause member function had first parameter is this so

#include <iostream>

class Demo {

	int x = 10;
	public:
	int getData() const {
	
		return x;
	}
};

std::ostream& operator<< (std::ostream& cout, const Demo& obj){

	cout<< obj.getData();

	return cout;
}

int main(){

	Demo obj;
	std::cout << obj << std::endl;
	return 0;
}

