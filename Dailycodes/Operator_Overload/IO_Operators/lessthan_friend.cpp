// less than using Friend function

#include <iostream> 

class Demo {
	
	int x;

	public:
		friend std::istream& operator>>(std::istream& cin, Demo& obj);

		friend int operator<(const Demo& obj1, const Demo& obj2);

		void printData()const{
		
			std::cout << x << std::endl;
		}

		int getA(Demo& obj1) const {
		
			return obj1.x;
		}
		int getB(Demo& obj2) const{
		
			return obj2.x;
		}
};


int operator<(const Demo& obj1, const Demo& obj2){

	return (obj1.getA(obj1) < obj2.getB(obj2));
}

std::istream& operator>>(std::istream& cin, Demo& obj){

	cin>> obj.x;
	return cin;
}

int main(){

	Demo obj1;
	Demo obj2;

	std::cout << "Enter value" << std::endl;

	std::cin>>obj1;
	std::cin<<obj2;
	
	obj1.printData();
	obj2.printData();
	std::cout << "ans "<< (obj1 < obj2) << std::endl;
	
	return 0;

}
