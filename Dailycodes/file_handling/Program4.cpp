// when we create another object of file then it will truncate the file and then it will newly write into the file
//
// but when we write appends mode at that time it will adds the data end of the file 
// it will not truncate it

#include <iostream>
#include <fstream>

int main(){

	std::ofstream outfile("Incubator.txt");
	// std::ofstream outfile("Incubator.txt",std::ios::out);

	outfile << ("Vaibhav\n");
	outfile << ("Pranav\n");
	outfile << ("Sagar\n");

	outfile.close();

	std::ofstream outfile1("Incubator.txt",std::ios::app);
	outfile1 << ("Abhii\n");   
		
}
