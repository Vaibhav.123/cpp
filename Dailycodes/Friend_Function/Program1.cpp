// Friend Function
//

#include <iostream>

class Demo {

	int x = 10;

	protected:
	int y = 20;

	public:
		Demo(){
		
			std::cout << "Constructor" << std::endl;
		}
		void getData(){
		
			std::cout << "x = "<< x << std::endl;
			std::cout << "y = "<< y << std::endl;
		}
		friend void accessData(Demo&);
};

void accessData (Demo& obj){

	std::cout << "x = "<< obj.x << std::endl;
	std::cout << "y = "<< obj.y << std::endl;
	obj.getData();
}

int main(){

	Demo obj;
	accessData(obj);;
}
