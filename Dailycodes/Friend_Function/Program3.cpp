

#include <iostream>

class Demo {

	int x = 10;
	protected:
	int y = 20;

	public:
		Demo (){
		
			std::cout << "Constructor" << std::endl;
		}
		// error :  we cant give the body of friend function in that class 
		// only declaration of friend function is allowed
		friend void accessData(Demo& obj){
		
			std::cout << "X = "<< x << std::endl;
			std::cout << "Y = "<< y << std::endl;
		}
};

int main(){

	Demo obj;
	obj.accessData(obj);
}
