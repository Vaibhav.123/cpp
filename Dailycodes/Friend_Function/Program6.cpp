// One of the member function of one class is another class's friend function
// we want to make friend function of any class's member function then that class shoulb be complete
// that is it should be written firstly 
//
#include <iostream>
class Two;

class One {
	public:
		One(){
			std::cout << "One Constructor" << std::endl;
		}
		void accessData(const Two& obj);  // body will out of the class
};

class Two {
	int x = 10;
	protected:
	int y = 20;

	public:
		Two(){
			std::cout << "Two Constructor" << std::endl;
		}

		friend void One::accessData(const Two& obj);
};

void One::accessData(const Two& obj){

	std::cout << "X = " << obj.x << std::endl;
	std::cout << "Y = " << obj.y <<  std::endl;
}

int main(){

	One obj1;
	Two obj2;
	obj1.accessData(obj2);  // call to the friend function
}
