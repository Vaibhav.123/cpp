// friend Class
//

#include <iostream>
class Two;   // it is optional forward declaration
	     // because there is no need we does create any instance of that class 
	     // so there is no error so it is optional
class One {
	int x = 10;
	protected:
	int y = 20;
	public :
	
	One(){
	
		std::cout << "One Constructor" << std::endl;
	}
	friend class Two;
};

class Two {

	public :
	Two(){
		std::cout << "Two Constructor" << std::endl;
	}
	private:
		void getData(const One& obj){

			std::cout << "X = " << obj.x << std::endl;
			std::cout << "Y = " << obj.y << std::endl;
		}
	public:
		void accessData(const One& obj){
		
			std::cout << "X = " << obj.x << std::endl;
			std::cout << "Y = " << obj.y << std::endl;
			getData(obj);
		}
};

int main(){

	One obj1;
	Two obj2;
	obj2.accessData(obj1);

}
