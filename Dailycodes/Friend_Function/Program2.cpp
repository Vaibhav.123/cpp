// swap using friend function

#include <iostream>

class Demo {

	int x = 10;
	protected:
       	int y = 20;

	public: 
		Demo(){
		
			std::cout << "Constructor" << std::endl;
		}
		void getData(){
		
			std::cout << "X = "<< x << std::endl;
			std::cout << "Y = "<< y << std::endl;
		}
		friend void swapData(Demo&);
};

void swapData(Demo& obj){
	
	obj.getData();
	int temp = obj.x;
	obj.x = obj.y;
	obj.y = temp;
	obj.getData();

}
int main(){

	Demo obj;
	swapData(obj);
}

