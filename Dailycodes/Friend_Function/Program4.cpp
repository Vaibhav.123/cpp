// if we write code of two classes and one friend function then we have to declare forward declaration of next class
// here in this code 
// there is only one friend function and it access the both classes private and protected variables at a time
//
#include <iostream>

class Two;   // forward declaration 
class One {

	int x = 10;
	protected:
	int y = 20;

	public:
		One(){
		
			std::cout << "One Constructor" << std::endl;
		}
	private:
		void getData() const {  // we have make this method const because our reference will be const
		
			std::cout << "X = " << x << std::endl;
			std::cout << "Y = " << y << std::endl;
		}
		friend void accessData(const One& ,const Two&);
};

class Two {

	int a = 30;
	protected:
	int b = 40;

	public:
		Two(){
		
			std::cout << "Two Constructor" << std::endl;
		}
	private:
		void getData() const {  // we have make this method const if it is private
		
			std::cout << "A = " << a << std::endl;
			std::cout << "B = " << b << std::endl;
		}
		friend void accessData(const One& ,const Two&);
};

void accessData(const One& obj1, const Two& obj2){

	obj1.getData();
	obj2.getData();
}

int main(){

	One obj1;
	Two obj2;
	accessData(obj1, obj2);
}
