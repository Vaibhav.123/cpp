

// Sceanario 3:
// passing obj as parameter to the meber function at that time copy constructor gets called


#include <iostream>

class Demo {
	
	int x = 10;
	int y = 20;
	public:
	Demo(){
	
		std::cout << "In Constructor" << std::endl;
		std::cout << x <<"  "<< y << std::endl;   //  10  20
	}
	Demo(int x,int y){
		
		this->x = x;
		this->y = y;

		std::cout << "In Para" << std::endl;
		std::cout << x <<"  "<< y << std::endl;   // 100  200
	}

	Demo(Demo &obj){
	
		std::cout << "In Copy" << std::endl;
	}

	void info(Demo obj){   // Demo obj = obj1  Copy Constructor gets called
	
		std::cout << x <<"  "<< y << std::endl;   // 100  200
		std::cout << obj.x <<"  "<< obj.y << std::endl;    // 10  20
		
	}
};

int main(){

	Demo obj1;
	Demo obj2(100,200);
	obj2.info(obj1);
	return 0;
}

// OP:
// In Constructor
// 10  20
// In para
// 100  200
// In Cpoy
// 100  200
// 10  20
