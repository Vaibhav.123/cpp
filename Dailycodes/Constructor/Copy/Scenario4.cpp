// at the time of returning reference of object from member function


#include <iostream>

class Demo {
	
	int x = 10;
	int y = 20;
	
	public:
	Demo(){
	
		std::cout << "In Constructor" << std::endl;
	}
	Demo(int x, int y){
		
		this->x = x;
		this->y = y;
		std::cout << "In Para" << std::endl;
		std::cout << x <<"  "<< y << std::endl;
	}

	Demo(Demo &ref){
	
		std::cout << "In Copy" << std::endl;
	}

	Demo& info(Demo &obj){
	
		obj.x = 500;
		obj.y = 600;

		return obj;
	}

	void access(){
	
		std::cout << x <<"  "<< y << std::endl;

	}
};

int main(){

	Demo obj1;
	obj1.access();

	Demo obj2(100,200);
	obj2.access();
	Demo obj3 = obj2.info(obj2);
	obj3.access();
}
// OP:
// In Constructor
// 10  20
// In Para
// 100  200
// 100  200
// In Copy
// 10  20
