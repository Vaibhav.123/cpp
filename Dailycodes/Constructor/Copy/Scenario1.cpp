
//  1st scenario :
//  in main when we copy object into another object then copy constructor gets called
//  call from outside the class

#include <iostream> 

class Demo {
	public :
	Demo(){
	
		std::cout << "In Constructor" << std::endl;
	}
	Demo(int x){
	
		std::cout << "In Para" << std::endl;
	}
	Demo(Demo &obj){
	
		std::cout << "In Copy" << std::endl;
	}
};

int main(){

	Demo obj1;

	Demo obj2 = obj1;
	
	return 0;
}
