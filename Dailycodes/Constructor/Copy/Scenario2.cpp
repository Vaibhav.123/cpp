
// return value
//  when we create object in constructor at that time
//
#include <iostream>

class Demo {

	public:

		Demo(){
		
			std::cout << "In Constructor" << std::endl;

		}

		Demo (int x){

			Demo obj1;
			Demo obj2 = obj1;
			std::cout << "In Para" << std::endl;
		}

		Demo(Demo &obj){
		
			std::cout << "In Copy" << std::endl;
		}
};

int main(){

	Demo obj1(10);

	return 0;
	
}
//OP:
//In constructor
//In Cpoy
//In Para
