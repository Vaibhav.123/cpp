
// Scenario 5:
// in array 
#include <iostream>

class Demo {

	public:
		Demo(){
		
			std::cout << "In Constructor" << std::endl;
		}
		Demo(int x){
		
			std::cout << "In para" << std::endl;
		}
		Demo(Demo &obj){
		
			std::cout << "In Copy" << std::endl;
		}
};

int main(){

	Demo obj1;
	Demo obj2;
	Demo obj3;

	Demo arr[] = {obj1,obj2,obj3};
	// Demo arr[0] = obj1;
	// Demo arr[1] = obj2;
	// Demo arr[2] = obj2;
	
}
//OP:
//  In Constructor
//  In Constructor
//  In Constructor
//  In Copy
//  In Copy
//  In Copy
