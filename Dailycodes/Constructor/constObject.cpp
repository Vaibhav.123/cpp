
// object const kela tr aatla data change nahi karu shkt apan
// aani change kela tr constructor madhech hoto
//

#include <iostream>

class Demo {

	public:
		int x = 10;

		Demo(){
		
			this->x = 80;
			std::cout << "In Constructor" << std::endl;
		}

		Demo(int x) {
		
			this->x = x;
		}

		void getData() const {  // function constant hot
		
			std::cout << x << std::endl;
			// this->x = 80;  // 
		}
		// jrr member function const nahi kel tr error yeto karan member function ne suddha apan baherun value aanun aatil
		// instance variable chi value change karu shkto tyamul error yeto
};

int main(){
	
	const Demo obj;  

	std::cout << obj.x << std::endl;
	obj.getData();
	
	Demo(100);
	obj.getData();
	
}
