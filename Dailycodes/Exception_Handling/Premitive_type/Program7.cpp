// for sqrt function exception will be handled
// for minus value exception will be there
#include<iostream>
#include<cmath>

class SqrtDemo {

	int x;
	public :
		SqrtDemo(int x) {
		
			this->x = x;
		}
		void findSqrt(){
			
			try {
				if(x < 0)
					throw "Illegal Operation";
				else 
					std::cout << sqrt(x) << std::endl;
			}catch(const char* str){
			
				std::cout << str << std::endl;
			}
			std::cout << "End Sqrt" << std::endl;	
		}
};
int main(){
	
	int x;
	std::cout << "Enter num :" << std::endl;
	std::cin >> x;

	SqrtDemo obj(x);

	obj.findSqrt();

	std::cout << "End Main" << std::endl;
	return 0;
}
