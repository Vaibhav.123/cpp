// this the concept of rethrowing exception 
#include <iostream>

void EleSearch(int arr[], int size, int ele){
	
	int flag = 0;
	int i;

	for(i=0; i<size; i++){
	
		if(arr[i] == ele){
			flag=1;
			break;
		}
	}
	try{
		if(flag == 1){
	
			std::cout << "Element found at Index :" << i << std::endl;
		}else{
	
			throw "Number Not Found";
		}
	}catch(const char *str){
	
		std::cout << str << std::endl;
		throw;    // rethrowing exception here 
	}
}
int main(){

	int arr[] = {10,20,30,40,50};

	int search;
	std::cout << "ENter for serach" << std::endl;
	std::cin >> search;
	
	int size = sizeof(arr)/sizeof(arr[0]);
	try{
	
		EleSearch(arr,size,search);
	}catch(const char *str){
	
		std::cout << "Catch In Main : " << str << std::endl;
	}
	return 0;
}
