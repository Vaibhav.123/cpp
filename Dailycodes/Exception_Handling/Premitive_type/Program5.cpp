// In cpp for try block exact catch block is must there 
// we can also use ellipses in catch block 
// it can sotre any type of exception 
// but it is bad programming practice because using we cant understand the which type exception will be occured
//  for primitive types there must be exact catch block is there

#include <iostream>
int main(){

	try {
	
		throw 'A';
	}catch(int x){
	
		std::cout << "Int" << std::endl;
	}catch(char ch){
	
		std::cout << "Char" << std::endl;
	}
	try {
		throw "Hello";
	}catch(...){
	
		std::cout << "Ellipses" << std::endl;
	}
}
