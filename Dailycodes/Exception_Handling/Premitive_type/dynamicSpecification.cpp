// here in the below code dynamic Specification concept is there
// means in this code the fun function has to be restricted to only throws two type of peimitve exception
//  like in the code int and double other exeption cant be throw
//  this feature will be come in version 03
//  so for compiling this code in version 03 
//  in the latest version it will be deprecated 
//  command : g++ -std=c++03 Program.cpp
//
#include <iostream>
void fun(int x) throw(int,double){

	if(x == 1)
		throw -1;
	else if(x == 2)
		throw 'A';
	else if(x == 3)
		throw 4.5;
	else 
		throw "Other Exception";

}

int main(){

	int x;
	std::cout << "Enter value" << std::endl;
	std::cin >> x;

	try {
		fun(x);
	}catch(int x){
		std::cout << "Int" << std::endl;
	}catch(char ch){
		std::cout << "Char" << std::endl;
	}catch(double){
		std::cout << "Double" << std::endl;
	}

	std::cout << "End Main" << std::endl;
 
}
