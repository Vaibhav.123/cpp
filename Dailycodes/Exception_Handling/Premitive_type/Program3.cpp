// for exception handling three keywords are used 
// try, catch, throw
// these are used for converting to abnormal termination to normal termination
// in this primitive types are to be thrown using throw keyword 

#include<iostream>

int main(){

	std::cout << "Start Main" << std::endl;
	try {
	
		throw 'A';
	}catch(int x){
	
		std::cout << "Handling" << std::endl;
	}

	std::cout << "End Main" << std::endl;

	return 0;
}
// here abnormal flow is there 
// because there is no matching catch block for try 
/* OP: 
Start Main
terminate called after throwing an instance of 'char'
Aborted

*/
