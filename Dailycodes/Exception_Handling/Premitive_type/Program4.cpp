

#include <iostream>
int main(){

	int x = 10;
	int y = 0;
	
	std::cout << "Start Main" << std::endl;
	try {
	
		if(y != 0)
			std::cout << x/y << std::endl;
		else 
			throw " / by zero";
	}catch(const char* str){
	
		std::cout << "Exception :" << str << std::endl;
	}

	std::cout << "End Main" << std::endl;
}
/*
Start Main
Exception : / by zero
End Main
*/
