// if we cant use the try catch here then output is means there abnornmal terminbation is there
// 
/*
Start Main
Start Fun
Start Gun
terminate called after throwing an instance of 'int'
Aborted
*/

#include <iostream>

class Demo {

	public :
		void gun(){
		
			std::cout << "Start Gun" << std::endl;
			throw -1; 
			std::cout << "End Gun" << std::endl;
		}

		void fun(){
		
			std::cout << "Start Fun" << std::endl;
			gun();

			std::cout << "End Fun" << std::endl;
		}
};
int main(){
	
	Demo obj;
	std::cout << "Start Main" << std::endl;
	
	try{
		obj.fun();
	}catch(...) {
	
		std::cout << "Handling" << std::endl;
	}

	std::cout << "End Main" << std::endl;
	return 0;
}
// now using try catch normal flow is there 
// means it will reach to the return 0 statement
// so output will be
/*
	Start Main
	Start Fun
	Start Gun
	Handling
	End Main
*/
