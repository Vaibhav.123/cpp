
// if we write typename in the template only one then 
// the paramters data type will be same otherwise it gives error
// so for only one functions paramters are of diff type we have to write int the template 
// two typename 
// so it can select it.
//
#include <iostream>

template <typename T, typename U>

// here auto is used for any return value but only one return value it will return 
//
auto min(T x, U y){

	/* if we write this in the if else then this error will be occured
	 error: inconsistent deduction for auto return type: ‘int’ and then ‘float’
                  // return y;
	 this is drawback of this 
	
	if(x<y)
		return x;
	else
		return y;
		*/
	// so for that purpose cpp can handle this using ternary operator
	
	return (x<y)? x:y;
}

int main(){

	std::cout << min(15,7.5f) << std::endl; 	// 7.5
	
	std::cout << min('A',100) << std::endl;		// 65
}
