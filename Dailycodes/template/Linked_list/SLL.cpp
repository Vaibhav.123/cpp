

#include<iostream>
template <typename T>
struct Node {

	int data;
	Node *next;
};

template <class T>
class LinkedList {
	
	public:
		Node<T> *head = NULL;

		Node<T> *createNode() {
			
			Node<T> *newNode = new Node<T>;
			std::cout << "Enter data = " << std::endl;
			std::cin >> newNode->data ;

			newNode->next = NULL;
			return newNode;

		}
		void addNode(){
			

			Node<T> *newNode = createNode();
			if(head == NULL) {
				head = newNode;
			}else{
			
				Node<T> *temp = head;

				while(temp->next != NULL){
				
					temp = temp->next;
				}

				temp->next = newNode;
			}

		}

		void printSLL(){
		
			Node<T> *temp = head;

			while(temp->next != NULL){
			
				std::cout<< temp->data << " => " ;
				temp=temp->next;
			}
			std::cout<< temp->data << std::endl;
		}

		void addFirst(){
			
			Node<T> *newNode = createNode();
			if(head==NULL){
				head=newNode;
			}else {
			
				newNode->next = head;
				head=newNode;
			}
		}

		void addAtPos(int pos) {
		
			Node<T> *temp = head;
			int count=0;
			while(temp!=NULL){
				temp=temp->next;
				count++;
			}
			temp=head;
			if(head == NULL){
				Node<T> *newNode = createNode();
				head = newNode;
			}
			else if(pos==1){
				addFirst();
			}else if(pos==count+1){
				addNode();
			}else{
			
				Node<T> *newNode = createNode();
				while(pos!=2){
					
					temp=temp->next;
					pos--;
				}
				newNode->next=temp->next;
				temp->next=newNode;
			}
		}

		int count(){
		
			Node<T> *temp = head;
			int count=0;
			while(temp!=NULL){
			
				count++;
				temp=temp->next;
			}
			return count;
		}
};


int main(){

	int ch;
	char choice;
	LinkedList<int> obj;

	do{
	
		std::cout << "1.addNode" << std::endl;
		std::cout << "2.addFirst" << std::endl;
		std::cout << "3.printSLL" << std::endl;
		std::cout << "4.addAtPos" << std::endl;
		std::cout << "5.Count" << std::endl;
		std::cout << "6.deleteLast" << std::endl;

		std::cout << "Enter your choice : " ;
		std::cin>> ch;

		switch(ch){
				
			case 1:
				{
					obj.addNode();
					break;
				}
			case 2:
				{
					obj.addFirst();
					break;
				}
			case 3:
				{
					obj.printSLL();
					break;
				}
			case 4:
				{
					int pos;
					std::cout << "Enter pos : ";
					std::cin >> pos;
					obj.addAtPos(pos);
					break;
				}
			case 5:
				{
					int count = obj.count();
					std::cout << "Count = "<< count <<std::endl;
					break;
				}
			default :
				std::cout<< "Wrong input" << std::endl;
				break;

		}
		std::cout<< "do you want to continue : "<<std::endl;
		std::cin>> choice;


	}while(choice == 'y' || choice == 'Y');

}
