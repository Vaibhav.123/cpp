
// in the below code there are multiple functions are witten but the body of functions will be same 
// so in this code redundency is more
// so for this purpose template is in the picture 
// we can see in the next code
//
#include <iostream>

char min(char x,char y){

	return (x<y)? x:y;
}

int min(int x,int y){

	return (x<y)? x:y;
}
/*
float min(float x,float y){

	return (x<y)? x:y;
}

double min(double x,double y){

	return (x<y)? x:y;
}*/
int main(){

	std::cout << min('A','B') << std::endl;
	std::cout << min(10.5f,20.5f) << std::endl;
	//std::cout << min(10.5f,20.5f) << std::endl;
	//std::cout << min(30.5,20.5) << std::endl;
}
