
#include <iostream>
/*  
 *
template <typename T>
T min(T x, T y){

	return (x<y)? x:y;
}*/

template <typename T>
T min(T x, T y);
// this is the normal function 
char min(char x,char y){

	std::cout <<"Normal" << std::endl;
	return (x<y)? x:y;
}

// this is template 
template <>
char min (char x,char y){

	std::cout <<"Template" << std::endl;
	return (x<y)? x:y;
}

// this is normal function
int min(int x,int y){

	std::cout <<"Normal"<< std::endl;
	return (x<y)? x:y;
}

// this is template function
template <>
int min<int>(int x,int y){

	std::cout <<"Template"<< std::endl;
	return (x<y)? x:y;
}

// normal function
double min(double x,double y){

	std::cout <<"Normal"<< std::endl;
	return (x<y)? x:y;
}

// template function written by the compiler
template <>
double min<double>(double x,double y){

	std::cout <<"Template"<< std::endl;
	return (x<y)? x:y;
}

int main(){
	// here this called is to the template function
	std::cout << min<>('A','B') << std::endl;

	// and this called to the normal function
	std::cout << min('C','D') << std::endl;

	// this is also normal function call
	std::cout << min(10,20) << std::endl;

	// this call to the template function
	std::cout << min<int>(30,40) << std::endl;
	
	// this call to the template function
	std::cout << min<double>(10.5,20.5) << std::endl;
	
	// this call to the normal function
	std::cout << min(30.5,50.5) << std::endl;
	return 0;
}
