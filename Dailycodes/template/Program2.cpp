
// here in the below code we can see that code redundency is decrease but that is there
// if we write template then compiler own write that method in code
// it will writes methods depend on the paramters given
//
//
#include <iostream>

//template <typename T>
// this is the template of our code 
// 
/*
T min(T x, T y){

	return (x<y)? x:y;
}*/
// here compiler writes the code as in the format :
// this is proof of that compilers writes those methods internally when we write template
//
template <typename T>
T min(T x, T y);

template <>

char min <char>(char x,char y) {

	std::cout <<"Template"<<std::endl;
	return (x<y)? x:y;
}

template <>
int min <int>(int x,int y) {

	std::cout <<"Template"<<std::endl;
	return (x<y)? x:y;
}

template <>
float min <float>(float x,float y) {

	std::cout <<"Template"<<std::endl;
	return (x<y)? x:y;
}

template <>
double min <double>(double x,double y) {

	std::cout <<"Template"<<std::endl;
	return (x<y)? x:y;
}
int main(){

	std::cout << min('A','B') << std::endl;		// A
	std::cout << min(10,20) << std::endl;		// 10
	std::cout << min(10.5f,20.5f) << std::endl;	// 10.5
	std::cout << min(30.5,40.5) << std::endl;	// 30.5
}
