//plus point of this array is we can pass array to function then at that time it will directly create the new array 
//in the function but not using pointer it will 
// means pointer is not there from that function 

#include<iostream>
#include<array>
/*
 * here for this error will occured
void fun(int arr[]) {

	std::cout << sizeof(arr) << std::endl;
}
*/
void fun(std::array<int,5> arr){

	std::cout << sizeof(arr) << std::endl;
}
int main(){
	// here size for array must required because array is fixed array
	std::array<int,5> arr = {10,20,30,40,50};

	std::cout << sizeof(arr) << std::endl;

	fun(arr);

	return 0;
}
// error : because in main array is class array and we can store that array in ant type of array
// so it cant be converted into int*
