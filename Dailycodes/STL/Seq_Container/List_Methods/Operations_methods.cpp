// Operations methods on list

#include<iostream>
#include<list>

std::_List_iterator<int> operator +(std::_List_iterator<int> obj, int index){

	while(index){
	
		obj++;
		index--;
	}
	return obj;
}

int main() {
	
	std::list<int> list1 = {10,20,70};
	std::list<int> list2 = {5};
	
	// splice()
	std::cout << "splice(pos, list)" << std::endl;
	std::list<int> :: iterator itr;
	list2.splice(list2.begin()+1,list1);
	for(itr = list2.begin(); itr != list2.end(); itr++){
		std::cout << *itr << "  ";
	}
	std::cout << std::endl;
	
	std::cout << "splice(pos, list, ele)" << std::endl;
	list1 = {30,40,50,60};

	list2.splice(list2.begin()+3,list1,list1.begin()+0);
	for(itr = list2.begin(); itr != list2.end(); itr++){
		std::cout << *itr << "  ";
	}
	std::cout << std::endl;

	std::cout << "splice(pos, list, first, last)" << std::endl;
	list2.splice(list2.begin()+4,list1,list1.begin()+0, list1.end());
	
	for(itr = list2.begin(); itr != list2.end(); itr++){
		std::cout << *itr << "  ";
	}
	std::cout << std::endl;
	
	// list2 = {5  10  20  30  40  50  60  70}
	
	// remove()
	std::cout << "remove(5)" << std::endl;
	list2.remove(5);
	for(itr = list2.begin(); itr != list2.end(); itr++){
		std::cout << *itr << "  ";
	}
	std::cout << std::endl;
	list2 = {10,10,30,30,20,20,50};
	
	
	// unique() remove all the unique elements from the list
	std::cout << "list2 is" <<std::endl;

	for(itr = list2.begin(); itr != list2.end(); itr++){
		std::cout << *itr << "  ";
	}
	std::cout << std::endl;
	std::cout << "unique " << std::endl;
	list2.unique();
	for(itr = list2.begin(); itr != list2.end(); itr++){
		std::cout << *itr << "  ";
	}
	std::cout << std::endl;

	// sort()
	
	std::cout << "Sort" << std::endl;
	list2.sort();
	for(itr = list2.begin(); itr != list2.end(); itr++){
		std::cout << *itr << "  ";
	}
	std::cout << std::endl;
	
	// merge()
	
	std::list<int> list3 = {40,60};
	std::cout << "Merge" <<std::endl;
	list3.merge(list2);
	for(itr = list3.begin(); itr != list3.end(); itr++){
		std::cout << *itr << "  ";
	}
	std::cout << std::endl;

	// reverse()
	
	std::cout << "reverse" << std::endl;
	list3.reverse();
	for(itr = list3.begin(); itr != list3.end(); itr++){
		std::cout << *itr << "  ";
	}
	std::cout << std::endl;	
}
