

// Modifiers methods on list
#include<iostream>
#include<list>

int main(){
	
	std::list<int> lst(5,0);

	std::list<int> lst1 = {10,20,30,40,50,60};

	std::list<int> :: iterator itr;
	
	std::cout << "1st list :" << std::endl;
	for(itr = lst.begin(); itr != lst.end(); itr++){
		std::cout << *itr << "  ";
	}
	std::cout << std::endl;

	std::cout << "2nd list :" << std::endl;
	for(itr = lst1.begin(); itr != lst1.end(); itr++){
		std::cout << *itr << "  ";
	}
	std::cout << std::endl;

	//  assign()  means assign directly new content to the previous list
	std::cout << "After Assign list" << std::endl;
	lst.assign(lst1.begin(), lst1.end());
	for(itr = lst.begin(); itr != lst.end(); itr++){
		std::cout << *itr << "  ";
	}
	std::cout << std::endl;

	// emplace_front() means construct and insert element at beg
	
	std::cout << "Emplace front" << std::endl;
	lst.emplace_front(5);
	for(itr = lst.begin(); itr != lst.end(); itr++){
		std::cout << *itr << "  ";
	}
	std::cout << std::endl;

	// emplace_back()  means construct and insert element at back
	
	std::cout << "Emplace_back" << std::endl;
	lst.emplace_back(70);
	for(itr = lst.begin(); itr != lst.end(); itr++){
		std::cout << *itr << "  ";
	}
	std::cout << std::endl;

	// emplace() means insert ele random
	std::cout << "Emplace lst.begin(),3" << std::endl;
	lst.emplace(lst.begin(),3);
	for(itr = lst.begin(); itr != lst.end(); itr++){
		std::cout << *itr << "  ";
	}
	std::cout << std::endl;

	// pop_front() and pop_back()
	std::cout << "Pop_Front and Pop_back" << std::endl;
	lst.pop_front();
	lst.pop_back();
	lst.pop_front();
	for(itr = lst.begin(); itr != lst.end(); itr++){
		std::cout << *itr << "  ";
	}
	std::cout << std::endl;
	
	// push_back() and push_front()
	std::cout << "push_Front and push_back" << std::endl;
	lst.push_front(5);
	lst.push_back(70);
	for(itr = lst.begin(); itr != lst.end(); itr++){
		std::cout << *itr << "  ";
	}
	std::cout << std::endl;
	
	// insert()
	std::cout << "insert(2,25) and insert(2,80, and insert(another list))" << std::endl;
	itr = lst.begin();
	++itr;
	++itr;
	lst.insert(itr,25);
	lst.insert(lst.end(),2, 80);
	std::list<int> lst2 = {100,200,300};
	lst.insert(itr,lst2.begin(),lst2.end());
	for(itr = lst.begin(); itr != lst.end(); itr++){
		std::cout << *itr << "  ";
	}
	std::cout << std::endl;

	// erase()
	std::cout << "Erase()" << std::endl;

	lst.erase(lst.begin());
	itr = lst.begin();
	itr++;
	itr++;
	lst.erase(itr);
	// lst.erase(lst.begin(), lst.end());
	for(itr = lst.begin(); itr != lst.end(); itr++){
		std::cout << *itr << "  ";
	}
	std::cout << std::endl;

	// swap()
	std::cout << "swap()" << std::endl;
	std::cout << "Before Swap list1 and list2" << std::endl;
	for(itr = lst.begin(); itr != lst.end(); itr++){
		std::cout << *itr << "  ";
	}
	std::cout << std::endl;
	std::list<int> lst3 = {100,200,300,400,500,600};
	for(itr = lst3.begin(); itr != lst3.end(); itr++){
		std::cout << *itr << "  ";
	}
	std::cout << std::endl;
	std::cout << "After Swap list1 is" << std::endl;
	lst.swap(lst3);
	for(itr = lst.begin(); itr != lst.end(); itr++){
		std::cout << *itr << "  ";
	}
	std::cout << std::endl;
	
	// resize()
	
	lst.resize(5);
	std::cout << "After resize :" << std::endl;
	for(itr = lst.begin(); itr != lst.end(); itr++){
		std::cout << *itr << "  ";
	}
	std::cout << std::endl;
	// clear()
	lst.clear();
	std::cout << "After clear" << std::endl;
	for(itr = lst.begin(); itr != lst.end(); itr++){
		std::cout << *itr << "  ";
	}
	std::cout << std::endl;
}
/*
OUTPUT : 

1st list :
0  0  0  0  0
2nd list :
10  20  30  40  50  60
After Assign list
10  20  30  40  50  60
Emplace front
5  10  20  30  40  50  60
Emplace_back
5  10  20  30  40  50  60  70
Emplace lst.begin(),3
3  5  10  20  30  40  50  60  70
Pop_Front and Pop_back
10  20  30  40  50  60
push_Front and push_back
5  10  20  30  40  50  60  70
insert(2,25) and insert(2,80, and insert(another list))
5  10  25  100  200  300  20  30  40  50  60  70  80  80
Erase()
10  25  200  300  20  30  40  50  60  70  80  80
swap()
Before Swap list1 and list2
10  25  200  300  20  30  40  50  60  70  80  80
100  200  300  400  500  600
After Swap list1 is
100  200  300  400  500  600
After resize :
100  200  300  400  500
After clear

*/
