// Capacity and element access methods in List 

#include<iostream>
#include<list>

int main(){

	std::list<int> lst = {10,20,30,40,50};

	//std::list<int> :: iterator itr;
	
	// empty()
	std::cout << "Empty or not" << std::endl;
	std::cout << lst.empty() << std::endl;   // 0

	// size()
	std::cout << "Size :" << std::endl;
	std::cout << lst.size() << std::endl;    // 5

	// max_size()
	std::cout << "Max_Size :" << std::endl;
	std::cout << lst.max_size() << std::endl;    // 384307168202282325
	
	// access methods 
	
	// front()
	std::cout << "Front :" << std::endl;
	std::cout << lst.front() <<std::endl;  // 10

	// back()
	std::cout << "Back :" << std::endl;
	std::cout << lst.back() <<std::endl;    // 50
}
