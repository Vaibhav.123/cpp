
//  Iterators Methods on list
#include<iostream> 
#include <list>

int main(){

	std::list<int> lst = {10,20,30,40,50};

	std::list<int> :: iterator itr1;
	
	// begin() and end()
	std::cout << "Using begin() and end()" << std::endl;
	for(itr1 = lst.begin(); itr1 != lst.end(); itr1++){
	
		std::cout << *itr1 << std::endl;
	}
	
	// rbegin() and rend()
	std::cout << "Using rbegin() and rend()" << std::endl;
	std::list<int> :: reverse_iterator itr2;
	for(itr2 = lst.rbegin(); itr2 != lst.rend(); itr2++){
	
		std::cout << *itr2 << std::endl;
	}

	// cbegin() and cend()
	std::cout << "Using cbegin() and cend()" << std::endl;
	std::list<int> :: const_iterator itr3;
	for(itr3 = lst.cbegin(); itr3 != lst.cend(); itr3++){
	
		std::cout << *itr3 << std::endl;
	}

	// crbegin() and crend()
	std::cout << "Using crbegin() and crend()" << std::endl;
	std::list<int> :: const_reverse_iterator itr4;
	for(itr4 = lst.crbegin(); itr4 != lst.crend(); itr4++){
	
		std::cout << *itr4 << std::endl;
	}
}
