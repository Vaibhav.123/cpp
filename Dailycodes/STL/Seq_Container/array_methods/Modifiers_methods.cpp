
// Modifiers 

#include<iostream>
#include<array>

int main(){
		
	std::array<int,5> arr = {10,20,30,40,50};
	std::cout << "before fill"  << std::endl;
	for(int i=0; i<arr.size(); i++){
		std::cout << arr[i] << "  ";
	}
	std::cout << std::endl;
	
	// fill()
	std::cout << "after fill" << std::endl;
	arr.fill(60);
	for(int i=0; i<arr.size(); i++){
		std::cout << arr[i] << "  ";
	}
	std::cout << std::endl;

	// swap()
	std::cout << " after swap" << std::endl;
	std::array<int,5> arr1 = {10,20,30,40,50};

	arr.swap(arr1);
	for(int i=0; i<arr.size(); i++){
		std::cout << arr[i] << "  ";
	}
	std::cout << std::endl;
	
} 
/* OUTPUT : 
before fill
10  20  30  40  50
after fill
60  60  60  60  60
swap
10  20  30  40  50
*/
