//  Iterators Methods on array
#include<iostream> 
#include <array>

int main(){

	std::array<int,5> arr = {10,20,30,40,50};

	std::array<int,5> :: iterator itr1;
	
	// begin() and end()
	std::cout << "Using begin() and end()" << std::endl;
	for(itr1 = arr.begin(); itr1 != arr.end(); itr1++){
	
		std::cout << *itr1 << std::endl;
	}
	
	// rbegin() and rend()
	std::cout << "Using rbegin() and rend()" << std::endl;
	std::array<int,5> :: reverse_iterator itr2;
	for(itr2 = arr.rbegin(); itr2 != arr.rend(); itr2++){
	
		std::cout << *itr2 << std::endl;
	}

	// cbegin() and cend()
	std::cout << "Using cbegin() and cend()" << std::endl;
	std::array<int,5> :: const_iterator itr3;
	for(itr3 = arr.cbegin(); itr3 != arr.cend(); itr3++){
	
		std::cout << *itr3 << std::endl;
	}

	// crbegin() and crend()
	std::cout << "Using crbegin() and crend()" << std::endl;
	std::array<int,5> :: const_reverse_iterator itr4;
	for(itr4 = arr.crbegin(); itr4 != arr.crend(); itr4++){
	
		std::cout << *itr4 << std::endl;
	}
}
/* OUTPUT :
Using begin() and end()
10
20
30
40
50
Using rbegin() and rend()
50
40
30
20
10
Using cbegin() and cend()
10
20
30
40
50
Using crbegin() and crend()
50
40
30
20
10
*/
