// capacity and element access  
// 

#include<iostream>
#include<array>

int main(){

	std::array<int,5> arr = {10,20,30,40,50};
	
	// empty()
	std::cout << "Empty or not" << std::endl;
	std::cout << arr.empty() << std::endl;   // 0

	// size()
	std::cout << "Size :" << std::endl;
	std::cout << arr.size() << std::endl;    // 5

	// max_size()
	std::cout << "Max_Size :" << std::endl;
	std::cout << arr.max_size() << std::endl;    // 5
	
	// access methods 
	
	// front()
	std::cout << "Front :" << std::endl;
	std::cout << arr.front() <<std::endl;  // 10

	// back()
	std::cout << "Back :" << std::endl;
	std::cout << arr.back() <<std::endl;    // 50

	// at()
	std::cout << "at method" << std::endl;
	std::cout << arr.at(2) << std::endl;     // 30

	// operator[]
	std::cout << "operator[]" << std::endl;
	std::cout << arr[4] << std::endl;     // 50

	// data()
	std::cout << "data" << std::endl;
	std::cout << (arr.data()) << std::endl;     // 0x7fff53c1f5c0
}
