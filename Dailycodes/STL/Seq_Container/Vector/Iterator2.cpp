
#include<iostream>
#include<vector>

int main(){

	std::vector <int> v = {10,20,30,40,50};

	std::vector <int> :: iterator itr;

	for(auto itr = v.begin(); itr != v.end(); itr++){
	
		std::cout << *itr << std::endl;
	}
	std::cout << "In reverse order" << std::endl;
	for(auto itr = v.rbegin(); itr != v.rend(); itr++){
	
		std::cout << *itr << std::endl;
	}
	return 0;
}
/*
 * 10
 * 20
 * 30
 * 40
 * 50
 * In reverse order
 * 50
 * 40
 * 30
 * 20
 * 10
 * */
