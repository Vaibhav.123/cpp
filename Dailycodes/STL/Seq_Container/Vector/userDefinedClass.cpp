
#include<iostream>
#include<vector>

class Player {

	int jerNo;
	std::string pName;

	public:
		Player(int jerNo, std::string pName){
		
			this->pName = pName;
			this->jerNo = jerNo;
		}

		void info(){
		
			std::cout << pName << " : " << jerNo << std::endl;
		}
};

int main(){

	Player obj1(18,"Virat");
	Player obj2(7,"MSD");
	Player obj3(45,"Rohit");

	std::vector<Player> vobj = {obj1, obj2, obj3};

	for(int i=0; i<vobj.size(); i++){
	
		vobj[i].info();
	}
	return 0;
} 
/* OUTPUT: 
Virat : 18
MSD : 7
Rohit : 45
*/
