// we can access list elements using index also 

#include<iostream>
#include<list>
// here ref is not required because in main there we required iterator type of object and from this function iterator type of object
// will return and in main we can dereference it so not required 
// 
std::list<int> :: iterator operator +(std::list<int> :: iterator obj, int index){

	while(index){
	
		++obj;
		index--;
	}
	return obj;
}
int main(){

	std::list<int> lst = {10,20,30,40,50};

	std::cout << *(lst.begin()+3) << std::endl;   // 40 
}
