

#include<iostream>
#include<list>

int main(){

	std::list<int> lst = {10,20,30,40,50};
	// std::list<int> lst(5,10);     // {10,10,10,10,10}

	std::list<int> :: iterator itr;

	lst.push_back(60);
	lst.push_front(5);

	for(itr = lst.begin(); itr != lst.end(); itr++){
	
		std::cout << *itr << std::endl;
	}
}
