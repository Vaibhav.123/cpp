

#include<iostream>
#include<vector>

int main(){

        std::vector<int> v(5,20);

        std::vector<int> v1 = {30,40,50,60,70,80};

        std::vector<int> :: iterator itr;

        std::cout << "1st vector :" << std::endl;
        for(itr = v.begin(); itr != v.end(); itr++){
                std::cout << *itr << "  ";
        }
        std::cout << std::endl;

        std::cout << "2nd vector :" << std::endl;
        for(itr = v1.begin(); itr != v1.end(); itr++){
                std::cout << *itr << "  ";
        }
        std::cout << std::endl;

        //  assign()  means assign directly new content to vector
        std::cout << "After Assign vector" << std::endl;
        v.assign(v1.begin(), v1.end());
        for(itr = v.begin(); itr != v.end(); itr++){
                std::cout << *itr << "  ";
        }
        std::cout << std::endl;

        // emplace_back()  means construct and insert element at back

        std::cout << "Emplace_back" << std::endl;
        v.emplace_back(70);
        for(itr = v.begin(); itr != v.end(); itr++){
                std::cout << *itr << "  ";
        }
        std::cout << std::endl;

        // emplace() means insert ele random
        std::cout << "Emplace v.begin(),3" << std::endl;
        v.emplace(v.begin(),3);
        for(itr = v.begin(); itr != v.end(); itr++){
                std::cout << *itr << "  ";
        }
        std::cout << std::endl;

         // push_back() and push_front()
        std::cout << "push_Front and push_back" << std::endl;
        v.push_back(70);
        for(itr = v.begin(); itr != v.end(); itr++){
                std::cout << *itr << "  ";
        }
        std::cout << std::endl;

        // insert()
        std::cout << "insert(2,25) and insert(2,80, and insert(another vector))" << std::endl;
        itr = v.begin();
        ++itr;
        ++itr;
        v.insert(itr,25);
        v.insert(v.end(),2, 80);
        std::vector<int> v2 = {100,200,300};
        v.insert(itr,v2.begin(),v2.end());
        for(itr = v.begin(); itr != v.end(); itr++){
                std::cout << *itr << "  ";
        }
        std::cout << std::endl;

        // erase()
        std::cout << "Erase()" << std::endl;

        v.erase(v.begin());
        itr = v.begin();
        itr++;
        itr++;
        v.erase(itr);
        // erase ()
	
        for(itr = v.begin(); itr != v.end(); itr++){
                std::cout << *itr << "  ";
        }
        std::cout << std::endl;

        // swap()
        std::cout << "swap()" << std::endl;
        std::cout << "Before Swap vector and vector2" << std::endl;
        for(itr = v.begin(); itr != v.end(); itr++){
                std::cout << *itr << "  ";
        }
        std::cout << std::endl;
        std::vector<int> v3 = {100,200,300,400,500,600};
        for(itr = v3.begin(); itr != v3.end(); itr++){
                std::cout << *itr << "  ";
        }
        std::cout << std::endl;
        std::cout << "After Swap vector1 is" << std::endl;
        v.swap(v3);
        for(itr = v.begin(); itr != v.end(); itr++){
                std::cout << *itr << "  ";
        }
        std::cout << std::endl;

        // resize()

        v.resize(5);
        std::cout << "After resize :" << std::endl;
        for(itr = v.begin(); itr != v.end(); itr++){
                std::cout << *itr << "  ";
        }
        std::cout << std::endl;
        // clear()
        v.clear();
        std::cout << "After clear" << std::endl;
        for(itr = v.begin(); itr != v.end(); itr++){
                std::cout << *itr << "  ";
        }
        std::cout << std::endl;
}
/* OUTPUT :

1st vector :
20  20  20  20  20
2nd vector :
30  40  50  60  70  80
After Assign vector
30  40  50  60  70  80
Emplace_back
30  40  50  60  70  80  70
Emplace v.begin(),3
3  30  40  50  60  70  80  70
push_Front and push_back
3  30  40  50  60  70  80  70  70
insert(2,25) and insert(2,80, and insert(another vector))
3  30  100  200  300  25  40  50  60  70  80  70  70  80  80
Erase()
30  100  300  25  40  50  60  70  80  70  70  80  80
swap()
Before Swap vector and vector2
30  100  300  25  40  50  60  70  80  70  70  80  80
100  200  300  400  500  600
After Swap vector1 is
100  200  300  400  500  600
After resize :
100  200  300  400  500
After clear
*/
