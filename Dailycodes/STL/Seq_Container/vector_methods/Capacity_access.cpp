// element acces and capacity in vector 
#include<iostream>
#include<vector>

int main(){
	std::vector<int> v = {10,20,30,40,50};
	// empty()
	std::cout << "Empty or not" << std::endl;
	std::cout << v.empty() << std::endl;   // 0

	// size()
	std::cout << "Size :" << std::endl;
	std::cout << v.size() << std::endl;    // 5

	// max_size()
	std::cout << "Max_Size :" << std::endl;
	std::cout << v.max_size() << std::endl;    // 5
	
	// front()
	std::cout << "Front :" << std::endl;
	std::cout << v.front() <<std::endl;  // 10

	// back()
	std::cout << "Back :" << std::endl;
	std::cout << v.back() <<std::endl;    // 50

	// at()
	std::cout << "at method" << std::endl;
	std::cout << v.at(2) << std::endl;     // 30

	// operator[]
	std::cout << "operator[]" << std::endl;
	std::cout << v[4] << std::endl;     // 50

	// data()
	std::cout << "data" << std::endl;
	std::cout << (v.data()) << std::endl;     // 0x7fff53c1f5c0

	// shrink_to_fit	
	std::vector<int> vt(100);
	std::cout << "capacity :" << vt.capacity() << std::endl;
	
	vt.resize(50);
	std::cout << "capacity :" << vt.capacity() << std::endl;

	std::cout << "shrink_to_fit" << std::endl;
	vt.shrink_to_fit();
	std::cout << "capacity " << vt.capacity() << std::endl;
	 // reserve ()
	vt.reserve(10);
	std::cout << "reserve " << vt.capacity() << std::endl;
}
