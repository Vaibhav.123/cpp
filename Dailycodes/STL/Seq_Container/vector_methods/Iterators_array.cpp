// array Iterators
#include<iostream> 
#include <vector>

int main(){
	std::vector<int> v = {10,20,30,40,50};

	std::vector<int> :: iterator itr1;
	
	// begin() and end()
	std::cout << "Using begin() and end()" << std::endl;
	for(itr1 = v.begin(); itr1 != v.end(); itr1++){
	
		std::cout << *itr1 << std::endl;
	}
	
	// rbegin() and rend()
	std::cout << "Using rbegin() and rend()" << std::endl;
	std::vector<int> :: reverse_iterator itr2;
	for(itr2 = v.rbegin(); itr2 != v.rend(); itr2++){
	
		std::cout << *itr2 << std::endl;
	}

	// cbegin() and cend()
	std::cout << "Using cbegin() and cend()" << std::endl;
	std::vector<int> :: const_iterator itr3;
	for(itr3 = v.cbegin(); itr3 != v.cend(); itr3++){
	
		std::cout << *itr3 << std::endl;
	}

	// crbegin() and crend()
	std::cout << "Using crbegin() and crend()" << std::endl;
	std::vector<int> :: const_reverse_iterator itr4;
	for(itr4 = v.crbegin(); itr4 != v.crend(); itr4++){
	
		std::cout << *itr4 << std::endl;
	}
}
/* OUTPUT : 

Using begin() and end()
10
20
30
40
50
Using rbegin() and rend()
50
40
30
20
10
Using cbegin() and cend()
10
20
30
40
50
Using crbegin() and crend()
50
40
30
20
10
*/
