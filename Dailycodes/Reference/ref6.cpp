

#include <iostream>

int &fun(int x){

	int &y = x;
	return y; 

}
int main(){

	int x = 10;
	int ret = fun(x);

	std::cout<< ret <<std::endl;  // 10

	return 0;
}
