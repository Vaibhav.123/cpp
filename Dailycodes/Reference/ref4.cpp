

#include <iostream>

void fun(int &ref1, int &ref2){

	int temp = ref1;
	ref1 = ref2;
	ref2 = temp;

	std::cout<< ref1 <<std::endl;	// 20
	std::cout<< ref2 <<std::endl;	// 10
}
int main(){

	int x = 10;
	int y = 20;

	std::cout<< x <<std::endl;	// 10
	std::cout<< y <<std::endl;	// 20

	fun(x,y);
	std::cout<< x <<std::endl;	// 20
	std::cout<< y <<std::endl;	// 10
}
