

#include <iostream>

void fun1(int x, int y){

	std::cout<< x <<"  "<<y<<std::endl;
}
void fun1(int &ref1, int&ref2){

	std::cout<< ref1 <<"  "<<ref2<<std::endl;

}
int main(){

	int x = 10;
	int y = 20;

	fun1(x,y);	 // error :  ambiguity
}
