

#include <iostream>

int main(){

	int x = 10;
	int &y = x;
	int *ptr = &x;

	std::cout<< x << std::endl;	// 10
	std::cout<< y << std::endl;	// 10
	std::cout<< ptr << std::endl;	// 100
	std::cout<< &x << std::endl;	// 100
	std::cout<< &y << std::endl;	// 100
	std::cout<< &ptr << std::endl;	// 200

}
